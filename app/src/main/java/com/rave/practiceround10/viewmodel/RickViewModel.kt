package com.rave.practiceround10.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.practiceround10.model.RickRepo
import com.rave.practiceround10.model.local.RickCharacter
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Rick view model.
 *
 * @property repo
 * @constructor Create empty Rick view model
 */
@HiltViewModel
class RickViewModel @Inject constructor(private val repo: RickRepo) : ViewModel() {
    private val _characters: MutableLiveData<List<RickCharacter>> = MutableLiveData()
    val characters: LiveData<List<RickCharacter>> get() = _characters

    init {
        getRickCharacters()
    }

    /**
     * Get rick characters.
     *
     */
    private fun getRickCharacters() = viewModelScope.launch {
        _characters.value = repo.getRickCharacters()
    }
}
