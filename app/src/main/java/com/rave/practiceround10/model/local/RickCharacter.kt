package com.rave.practiceround10.model.local

/**
 * Rick character.
 *
 * @property id
 * @property name
 * @constructor Create empty Rick character
 */
data class RickCharacter(
    val id: Int,
    val name: String
)
