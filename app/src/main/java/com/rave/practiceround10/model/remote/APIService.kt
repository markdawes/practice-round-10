package com.rave.practiceround10.model.remote

import com.rave.practiceround10.model.remote.dtos.CharacterResponse
import retrofit2.http.GET

/**
 * Api service to connect to endpoints.
 *
 * @constructor Create empty A p i service
 */
interface APIService {

    @GET("character")
    suspend fun getRickCharacters(): CharacterResponse
}
