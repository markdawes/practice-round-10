package com.rave.practiceround10.model

import com.rave.practiceround10.model.local.RickCharacter
import com.rave.practiceround10.model.remote.APIService
import javax.inject.Inject

/**
 * Rick repo.
 *
 * @property service
 * @constructor Create empty Rick repo
 */
class RickRepo @Inject constructor(private val service: APIService) {

    /**
     * Get rick characters.
     *
     * @return
     */
    suspend fun getRickCharacters(): List<RickCharacter> {
        val characterDTOs = service.getRickCharacters().results!!
        return characterDTOs.map {
            RickCharacter(
                id = it.id!!,
                name = it.name!!
            )
        }
    }
}
