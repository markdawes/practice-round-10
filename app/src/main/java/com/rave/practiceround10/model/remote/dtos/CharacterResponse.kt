package com.rave.practiceround10.model.remote.dtos

import kotlinx.serialization.Serializable

@Serializable
data class CharacterResponse(
    val info: InfoDTO?,
    val results: List<ResultDTO>?
)
