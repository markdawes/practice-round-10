package com.rave.practiceround10.model.remote.dtos

import kotlinx.serialization.Serializable

@Serializable
data class ResultDTO(
    val created: String?,
    val episode: List<String>?,
    val gender: String?,
    val id: Int?,
    val image: String?,
    val location: LocationDTO?,
    val name: String?,
    val origin: OriginDTO?,
    val species: String?,
    val status: String?,
    val type: String?,
    val url: String?
)
