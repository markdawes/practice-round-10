package com.rave.practiceround10.model.remote.dtos

import kotlinx.serialization.Serializable

@Serializable
data class LocationDTO(
    val name: String?,
    val url: String?
)
