package com.rave.practiceround10.model.remote.dtos

import kotlinx.serialization.Serializable

@Serializable
data class InfoDTO(
    val count: Int?,
    val next: String?,
    val pages: Int?,
    val prev: String?
)
