package com.rave.practiceround10

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Main class for dagger/hilt.
 *
 * @constructor Create empty Rick application
 */
@HiltAndroidApp
class RickApplication : Application()
