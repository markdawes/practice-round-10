package com.rave.practiceround10.viewmodel

import com.rave.practiceround10.model.RickRepo
import com.rave.practiceround10.model.local.RickCharacter
import com.rave.practiceround10.util.CoroutinesTestExtension
import com.rave.practiceround10.util.InstantTaskExecutorExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.RegisterExtension

@OptIn(ExperimentalCoroutinesApi::class)
@ExtendWith(InstantTaskExecutorExtension::class)
internal class ViewModelTest {

    @RegisterExtension
    private val testExtension = CoroutinesTestExtension()
    private val repo: RickRepo = mockk()
    private val viewModel = RickViewModel(repo)

    @Test
    fun testInitialValue() = runTest(testExtension.dispatcher) {
        val characters = viewModel.characters.value
        Assertions.assertTrue(characters.isNullOrEmpty())
    }

    @Test
    fun testValueChange() = runTest(testExtension.dispatcher) {
        val result = listOf(
            RickCharacter(
                id = 1,
                name = "Rick"
            )
        )

        coEvery { repo.getRickCharacters() } coAnswers { result }
        viewModel.getRickCharacters()
        val characters = viewModel.characters.value
        Assertions.assertEquals(result, characters)
    }
}
