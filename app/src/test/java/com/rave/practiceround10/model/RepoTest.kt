package com.rave.practiceround10.model

import com.rave.practiceround10.model.local.RickCharacter
import com.rave.practiceround10.model.remote.APIService
import com.rave.practiceround10.model.remote.dtos.CharacterResponse
import com.rave.practiceround10.model.remote.dtos.ResultDTO
import com.rave.practiceround10.util.CoroutinesTestExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

internal class RepoTest {

    @RegisterExtension
    private val testExtension = CoroutinesTestExtension()
    private val service: APIService = mockk()
    private val repo = RickRepo(service)

    @Test
    fun testGetCharacters() = runTest(testExtension.dispatcher) {
        // Given
        val serviceResult = CharacterResponse(
            info = null,
            results = listOf(
                ResultDTO(
                    created = null,
                    episode = null,
                    gender = null,
                    id = 1,
                    image = null,
                    location = null,
                    name = "Rick",
                    origin = null,
                    species = null,
                    status = null,
                    type = null,
                    url = null
                )
            )
        )
        val expectedResult = listOf(
            RickCharacter(
                id = 1,
                name = "Rick"
            )
        )

        // When
        coEvery { service.getRickCharacters() } coAnswers { serviceResult }
        val result = repo.getRickCharacters()

        // Then
        Assertions.assertEquals(expectedResult, result)
    }
}
